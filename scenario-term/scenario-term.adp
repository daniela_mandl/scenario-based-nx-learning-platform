<!DOCTYPE html>
<%
   set host [ns_info hostname]
   set host localhost   
   set wsProtocol [expr {[ns_conn protocol] eq "http" ? "ws" : "wss"}]
   set port [ns_config [ns_driversection] port [ns_conn port]]
   set baseUrl [string trimright $host:$port/[ns_conn url]]
   set wsUri $wsProtocol://$baseUrl/connect
 %>
<html>
  <head>
    <title>Scenario terminal</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/css/jquery.terminal.css">
<!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="/js/jquery.mousewheel-min.js"></script>
    <script src="/js/jquery.terminal-0.8.8.js"></script>
    <script language="javascript" type="text/javascript">  
    var wsUri = '<%= $wsUri %>';
    var output;
    var state;
    var websocket = 0;
    var myterm;
    var myheader;
    var mygiven;
    var mythen;
    var mywhen;
    function init() {
      output = document.getElementById('output');
      state = document.getElementById('state');
      testWebSocket();
    }  
    function testWebSocket() { 
      if ("WebSocket" in window) {
	websocket = new WebSocket(wsUri);
      } else {
	websocket = new MozWebSocket(wsUri);
      }
      websocket.onopen 	= function(evt) { onOpen(evt) }; 
      websocket.onclose = function(evt) { onClose(evt) }; 
      websocket.onmessage = function(evt) { onMessage(evt) }; 
      websocket.onerror = function(evt) { onError(evt) }; 
    }

    function onOpen(evt)  { state.innerHTML = '<span style="color:green;">CONNECTED</span>';  }
    function onClose(evt) { state.innerHTML = '<span style="color:red;">DISCONNECTED</span>'; testWebSocket(); }  
    function onMessage(evt) 	{
      var result = window.eval(evt.data);
      if (result != undefined) {
         console.log(String(result));
      }
      writeToScreen("RESPONSE:", "blue", evt.data);
    }
    function onError(evt)	{ writeToScreen("ERROR:", "red", evt.data); }  
    function doSend(message)	{ websocket.send(message); writeToScreen("SENT:", "green",  message);}
    function checkSubmit(e)	{ if (e && e.keyCode ==13) { doSend($('#msg').val()); }}

    function solveTask() {
        myheader.style.color = "green";
    }
    function solveGiven() {
	mygiven.style.color = "green";
    }
    function solveThen() {
	mythen.style.color = "green";
    }
    function solveWhen() {
	mywhen.style.color = "green";
    }
    function unsolveTask() { 
	myheader.style.color = "black";
    }
    function unsolveGiven() {
	mygiven.style.color = "black";
    }
    function unsolveThen() {
	mythen.style.color = "black";
    }
    function unsolveWhen() {
	mywhen.style.color = "black";
    }
    function failedTask() { 
	myheader.style.color = "red";
    }
    function failedGiven() {
	mygiven.style.color = "red";
    }
    function failedThen() {
	mythen.style.color = "red";
    }
    function failedWhen() {
	mywhen.style.color = "red";
    }

    function writeToScreen(tag, color, message) {
      //console.log(color + " " + message);
      if (message !== undefined) {
        output.innerHTML = "<span style='color: " + color
	    + ";'>" + tag + '</span> '
            + message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
      }
    }
    function clearOutput() {
      myterm.clear();
    }
    $( document ).ready(function() { init()});
  </script>
</head>

<body role="document">

  <div class="container theme-showcase" role="main">
    <div class="page-header">
      <h1>Scenario-based NX learning environment</h1>
<!--      <p class="lead">running at  <%= $wsProtocol://$baseUrl %> </p>-->
        <p>This is a learning enironment using a Tcl interpreter via a jcubic-terminal.
	  (If cursor is not blinking - click on terminal to activate it). 
	<p/>
    </div>
    
    

    <div class="row">
    <div class="well">
    <article>
	<header><h2>Create features for a bank account</h2></header>
	<p>The following features depend on the following already established code:<p/>
	<p>nx::Class create Account {<p/>
	<p style="text-indent:20px;">:variable balance 0<p/>
	<p><p/>
	<p style="text-indent:20px;">:public method balance {} {<p/>
	<p style="text-indent:40px;">return ${:balance}<p/>
	<p style="text-indent:20px;">}<p/>
	<p>}<p/>
        <header id="header1"><h3>Feature: Create an account</h3></header>
	<p id=given1>Given there exist a class <i>Account</i><p/>
	<p id=when1>When an instance <i>a1</i> of class <i>Account</i> is created<p/>
        <p id=then1>Then the method <i>balance</i> of instance <i>a1</i> returns <i>0</i><p/>
        <div id="term_demo" data-kernel="feature1"></div>

	<script language="javascript" type="text/javascript">
	    jQuery(function($, undefined) {
	      $('#term_demo').terminal(function(command, term) {
		myterm = term;
		myheader = header1;
		mythen = then1;
		mywhen = when1;
		mygiven = given1;
		var kernel = $('#term_demo').data('kernel')
	      //  var kernel = $('#choose-kernel option:selected').data('kernel')
		if (kernel === undefined) {
		  kernel = '';
		} else {
		//  term.echo('Task '+kernel+' in use');
		}
		if (command !== '') {
		   websocket.send(JSON.stringify(['eval', command, kernel]));
		} else {
		   term.echo('');
		}
	    }, {
		greetings: 'NX Interpreter',
		name: 'js_demo',
		height: 200,
		prompt: '% '});
	    });
	    </script>
    </article>

    <article>
        <header id="header2"><h3>Feature: Make a deposit</h3></header>
	<p id=given2>Given the variable <i>balance</i> of instance <i>a1</i> is <i>0</i><p/>
	<p id=when2>When the method <i>deposit</i> of instance <i>a1</i> with the parameter <i>100</i> is called
        <p id=then2>Then the method <i>balance</i> of instance <i>a1</i> returns <i>100</i><p/>
        <div id="term_demo2" data-kernel="feature2"></div>
	<script language="javascript" type="text/javascript">
	    jQuery(function($, undefined) {
	      $('#term_demo2').terminal(function(command, term) {
		myterm = term;
		myheader = header2;
		mythen = then2;
		mywhen = when2;
		mygiven = given2;
		var kernel = $('#term_demo2').data('kernel')
	      //  var kernel = $('#choose-kernel option:selected').data('kernel')
		if (kernel === undefined) {
		  kernel = '';
		} else {
		//  term.echo('Task '+kernel+' in use');
		}
		if (command !== '') {
		   websocket.send(JSON.stringify(['eval', command, kernel]));
		} else {
		   term.echo('');
		}
	    }, {
		greetings: 'NX Interpreter',
		name: 'js_demo2',
		height: 200,
		prompt: '% '});
	    });
	    </script>
	<header id="header3"><h3>Feature: Make a withdrawal</h3></header>
	<p id=given3>Given the variable <i>balance</i> of instance <i>a1</i> is <i>100</i><p/>
	<p id=when3>When the method <i>withdraw</i> of instance <i>a1</i> with the parameter <i>80</i> is called
        <p id=then3>Then the method <i>balance</i> of instance <i>a1</i> returns <i>20</i><p/>
        <div id="term_demo3" data-kernel="feature3"></div>

	<header id="header4"><h3>Refine the Feature: Make a withdrawal</h3></header>
	<p id=given4>Given the variable <i>balance</i> of instance <i>a1</i> is <i>100</i><p/>
	<p id=when4>When the method <i>withdraw</i> of instance <i>a1</i> with the parameter <i>120</i> is called
        <p id=then4>Then the method <i>balance</i> of instance <i>a1</i> returns <i>100</i><p/>
        <div id="term_demo4" data-kernel="feature4"></div>
      </article>

	</div>
	<p>Hints:<p/>
	
	<p>use command "info commands" to see possible commands<p/>
      </div>
    <div style="margin: 5px 0px;">
      Status: <span id="state">Uninitialized</span>
      <button type="button" class="btn btn-default" onclick="clearOutput();">Clear Output</button>
      <div id="output"></div>
    </div>
    </div>

</body>
</html>
