#
# scenarioTerm.tcl 
#
#
# Gustaf Neumann, April 2015
#
package require nsf

#
# Get configured urls
#
set urls [ns_config ns/server/[ns_info server]/module/websocket/scenario-term urls]

if {$urls eq ""} {
    ns_log notice "websocket: no scenarioterm configured"
    return
}

#
# Register websocket scenarioterm under every configured url
#
foreach url $urls {
    ns_log notice "websocket: scenarioterm available under $url"
    ns_register_adp  GET $url [file dirname [info script]]/scenario-term.adp
    ns_register_proc GET $url/connect ::ws::scenarioterm::connect
}

namespace eval ws::scenarioterm {
    #
    # The proc "connect" is called, whenever a new websocket is
    # established.  The terminal name is taken form the url to allow
    # multiple independent terminal on different urls.
    #
    nsf::proc connect {} {
        set term [ns_conn url]
	set channel [ws::handshake -callback [list ws::scenarioterm::handle_input -ip [ns_conn peeraddr] -term $term]]
	ws::subscribe $channel $term
    }

    #
    # Whenever we receive a message from the terminal, evaluate it by
    # the scenarioterm::handler and send back the result to the terminal
    # via a json command. Notice that this makes both the frontend and
    # backend highly programmable which makes the protocol between the
    # webclient and protocol extensible.
    #
    # In this work-in-progress version the terminal object "myterm"
    # hardcoded and should be replaced such that multiple instances of
    # the terminal can be used on one page.
    #
    nsf::proc handle_input {{-ip ""} {-term "terminal"} channel msg} {
        if {[catch {set cmdList [json_to_tcl $msg]} errorMsg]} {
            ns_log error "json_to_tcl returned $errorMsg"
        }
        ns_log notice "received <$msg> -> $cmdList"
        if {[lindex $cmdList 0] in [::ws::scenarioterm::handler cget -supported]} {
            set info [::ws::scenarioterm::handler {*}$cmdList]
            ns_log notice "handler returned <$info>"
            set result [string map [list ' \\'] [dict get $info result]]
	    set scenario [string map [list ' \\'] [dict get $info scenario]]
            switch [dict get $info status] {
                ok  { 
			if {$scenario eq true} {
			    set reply "myterm.echo('$result'); solveTask();"
			} else {
			    set reply "myterm.echo('$result'); failedTask();"
			}
		    }
		setup {set reply "myterm.echo('setup configured'); unsolveTask();"}
                error {set reply "myterm.error('$result'); failedTask();"}                
            }
        } else {
            ns_log warning "command <$msg> not handled"
            set reply "myterm.error('unhandled request from client'); failedTask();"
        }
        set r [ws::send $channel [::ws::build_msg $reply]]
        ns_log notice "[list ws::send $channel $reply] returned $r"
        #::ws::scenariocast $term [::ws::build_msg [::ws::build_msg $reply]]
    }

    #
    # ::ws::scenarioterm::json_to_tcl
    #
    # Helper function to convert a json object (as produced by the
    # JSON.stringify() function) to a Tcl list. This handles just
    # escaped backslashes and double quotes.
    
    nsf::proc json_to_tcl {msg} {
        if {![regexp {^\[.*\]$} $msg]} {
            error "invalid message <$msg>"
        }
        set result ""
        set escaped 0
        set state listStart
        foreach char [split $msg ""] {
            #ns_log notice "state $state char $char escaped $escaped"
            switch $state {
                listStart {
                    if {$char ne "\["} {error "unexpected character $char, expected \["}
                    set state wordStart
                }
                wordStart {
                    if {$char eq "\""} {
                        set state doubleQuotedWord
                        set escaped 0
                        set word ""
                    } else {
                        error "expect only doublequoted words"
                    }
                }
                doubleQuotedWord {
                    if {$char eq "\""} {
                        if {$escaped == 0} {
                            lappend result $word
                            set state wordEnd
                        } else {
                            append word $char
                            set escaped 0
                        }
                    } elseif {$char eq "\\"} {
                        if {$escaped == 0} {
                            set escaped 1
                        } else {
                            append word $char
                            set escaped 0
                        }
                    } else {
                        append word $char
                    }
                }
                wordEnd {
                    if {$char eq ","} {
                        set state wordStart
                    } elseif {$char eq "\]"} {
                        set state end
                    } else {
                        error "unexpected character '$char' in state $state"
                    }
                }
                end {
                }
            }
        }
        return $result
    }


    
    #
    # Generic THREAD class, somewhat simplified version of xotcl-core,
    # ported to NX
    #
    nx::Class create THREAD {
        :property cmd:required
        :property recreate:boolean
        :property tid

        :method init {} {
            if {![ns_ictl epoch]} {
                # We are during initialization.
                set :initCmd {
                    package req nx
                }
            }
            append :initCmd {
                ns_thread name SELF
                ::nsf::exithandler set {
                    ns_log notice "[self] exits"
                }
            }
            regsub -all SELF ${:initCmd} [self] :initCmd
            append :initCmd \n\
                [list set ::currentThread [self]] \n\
                ${:cmd}
            set :mutex [thread::mutex create]
            ns_log notice "mutex ${:mutex} created"
            next
        }
        #
        # :public method getInitcmd {} {
        #     return ${:initCmd}
        # }
        # :object method recreate {obj args} {
        #     #
        #     # this method catches recreation of THREADs in worker threads 
        #     # it reinitializes the thread according to the new definition.
        #     #
        #     ns_log notice "recreating [self] $obj, tid [$obj exists tid]"
        #     if {![string match "::*" $obj]} { set obj ::$obj }
        #     $obj configure -recreate true
        #     next
        #     $obj configure -cmd [lindex $args 0]
        #     if {[nsv_exists [self] $obj]} {
        #         set tid [nsv_get [self] $obj]
        #         ::thread::send $tid [$obj getInitcmd]
        #         $obj configure -tid $tid
        #         my log "+++ content of thread $obj ($tid) redefined"
        #     }
        # }

        :public method do {args} {
            if {![nsv_exists [current class] [self]]} {
                #
                # lazy creation of a new slave thread
                #
                thread::mutex lock ${:mutex}
                if {![nsv_exists [current class] [self]]} {
                    set :tid [::thread::create]
                }
                nsv_set [current class] [self] ${:tid}

                set initcmd ${:initCmd}
                ::thread::send ${:tid} $initcmd
                set :tid [nsv_get [current class] [self]]
                thread::mutex unlock ${:mutex}
            } else {
                #
                # slave thread is already up and running
                #
                set :tid [nsv_get [current class] [self]]
            }
            ns_log notice "calling [current class] (${:tid}, [pid]) $args"
            thread::send ${:tid} $args
        }
    }


    #
    # Class Handler
    #
    # Generic superclass for terminal command handlers. The property
    # "supported" tells the terminal interface, what commands will be
    # handled at the backend of the websocket to provide some
    # protection.
    #
    nx::Class create Handler {
        :property supported:1..n
    }
    
    #
    # Class CurrentThreadHandler
    #
    # The CurrentThreadHandler evals the sent command in the current
    # connection thread. This is e.g. useful for debugging
    # OpenACS. Note that after every command in a connection thread,
    # global variables are deleted. So the behavior is different from
    # a normal Tcl shell.
    #
    nx::Class create CurrentThreadHandler -superclass Handler {
        
        :property {supported:1..n eval}
        
	:public method eval {arg kernel} {
            ns_log notice "[current class] eval <$arg> <$kernel>"
            :useKernel $kernel
            if {[catch {
                return [list status ok result [namespace eval :: $arg]]
            } errorMsg]} {
                return [list status error result $errorMsg]
            }
        }
    }
    # CurrentThreadHandlers create handler

    #
    # Class KernelThreadHandler
    #
    # The KernelThreadHandler somewhat mimics Jupyter/ipython notebook
    # "kernels". We create a seperate thread that will contain the
    # actual state from all commands of a session. The commands maybe
    # either be executed in
    #
    #  a) the main interpreter of the thread (which contains all the
    #     currently defined infrastructure of the blueprint), or
    #  b) in a named child interpreter (named by the property "kernel").
    #     A named kernels is activated via the method "useKernel"
    #
    nx::Class create KernelThreadHandler -superclass Handler {
        
        :property {supported:1..n eval }
        
        :property {kernel ""}
        :variable kernels ""

        :method init {} {
            THREAD create kernels -cmd [subst {
                ns_log notice "Kernel thread [self] is running"
            }]
        }

        :public method useKernel {name} {
            if {$name ni ${:kernels} && $name ne ""} {
                kernels do interp create $name
                # autoload nx, should be generalized
                kernels do interp eval $name package require nx
	#	kernels do interp eval $name package require XOTcl
#		kernels do interp eval $name namespace import ::xotcl::*
		#todo load scenario
		if {[catch {
			set f [open "/usr/features/$name.txt"]
			set :myscenario [read $f]
			#set lines [split [read $f] "\n"]
			close $f
			ns_log notice "[current class] initKernel <$name> scenario ${:myscenario}"
		} errorMsg]} {
		        ns_log notice "we got an error <$errorMsg> $::errorInfo"
  	        }
		:initPreconditions $name
                lappend :kernels $name
            }
	    set :kernel $name
        }
	:public method initKernel {name} {
            if {$name ne ""} {
		kernels do interp delete $name
		#todo delete old kernel - lreplace
                kernels do interp create $name
                # autoload nx, should be generalized
                kernels do interp eval $name package require nx
		#kernels do interp eval $name package require XOTcl
		#kernels do interp eval $name namespace import ::xotcl::*
		:initPreconditions $name
                lappend :kernels $name
            }
        }
        :public method initPreconditions {name} {
	    ns_log notice "[current class] initPreconditions <$name>"
	    if {$name ne ""} {
		set f [open "/usr/features/setup.txt"]
		set setup [read $f]
		close $f
    		set setup {"nx::Class create Account {
			:variable balance 0

			:public method balance {} {
			return ${:balance}
			}
			}"}
		kernels do interp eval $name {*}$setup
		if {$name eq "feature2"} {
		    set f [open "/usr/features/setup-$name.txt"]
		    set setup [read $f]
		    close $f
		    kernels do interp eval $name {*}$setup
		}
	    }
	}
	:public method testConditions {kernel} {
	    ns_log notice "[current class] testConditions <$kernel> scenario ${:myscenario}"
	    if {$kernel ne ""} {
		set result true
	    	if {$kernel eq "example2"} {
	   	    set arg {"if {$x == 3} {\
			 return true;\
			} else {\
			    return false;\
			}"}
		    set test [list interp eval ${:kernel} {*}$arg]
	            set result [kernels do {*}$test]
		}
	    } else {
		return true
	    }
	    return $result
	}
        :public method eval {arg kernel} {
            ns_log notice "[current class] eval <$arg> <$kernel>"
            
	    if {[string compare reset $arg] eq 0} {
		set info [:reset $kernel]
	    } else {
		    :useKernel $kernel
		    if {${:kernel} ne ""} {
		       set cmd [list interp eval ${:kernel} {*}$arg]
		    } else {
		        set cmd $arg
		    }
		   	 ns_log notice "[current class] executes <$cmd>"
		    if {[catch {
			
		        set info [list status ok result [kernels do {*}$cmd] scenario [:testConditions $kernel]]
		        #set info [list status ok result "result: " [kernels do {*}$cmd] " scenario result: " [:testConditions $kernel]]
			#set info [list status ok result [kernels do {*}$cmd]]
			#set testresult [:testConditions $kernel]
		    } errorMsg]} {
		        ns_log notice "we got an error <$errorMsg> $::errorInfo"
		        set info [list status error result $errorMsg scenario null]
		    }
            }
            return $info
        }
        :public method reset {kernel} {
            ns_log notice "[current class] reset <$kernel>"
            :initKernel $kernel
            set info [list status setup result "$kernel restarted" scenario false]
            return $info
        }

    }
  #  KernelThreadHandler create handler -kernel demo

#
# The class ScenarioKernelTreadHandler should handle a Scenario-like situation:
# After setup and preconditions, the input of the user is validated against a pre-condition
#
# The information about the scenario is safed in a text file.
#
    nx::Class create ScenarioKernelThreadHandler -superclass KernelThreadHandler {
    }
    ScenarioKernelThreadHandler create handler -kernel demo


}

#
# Local variables:
#    mode: tcl
#    tcl-indent-level: 4
#    indent-tabs-mode: nil
# End:

