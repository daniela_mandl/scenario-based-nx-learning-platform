<!DOCTYPE html>
<%
   set host [ns_info hostname]
   set host localhost   
   set wsProtocol [expr {[ns_conn protocol] eq "http" ? "ws" : "wss"}]
   set port [ns_config [ns_driversection] port [ns_conn port]]
   set baseUrl [string trimright $host:$port/[ns_conn url]]
   set wsUri $wsProtocol://$baseUrl/connect
 %>
<html>
  <head>
    <title>mini term</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/css/jquery.terminal.css">
<!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="/js/jquery.mousewheel-min.js"></script>
    <script src="/js/jquery.terminal-0.8.8.js"></script>
    <script language="javascript" type="text/javascript">  
    var wsUri = '<%= $wsUri %>';
    var output;
    var state;
    var websocket = 0;
    var myterm;
    function init() {
      output = document.getElementById('output');
      state = document.getElementById('state');
      testWebSocket();
    }  
    function testWebSocket() { 
      if ("WebSocket" in window) {
	websocket = new WebSocket(wsUri);
      } else {
	websocket = new MozWebSocket(wsUri);
      }
      websocket.onopen 	= function(evt) { onOpen(evt) }; 
      websocket.onclose = function(evt) { onClose(evt) }; 
      websocket.onmessage = function(evt) { onMessage(evt) }; 
      websocket.onerror = function(evt) { onError(evt) }; 
    }

    function onOpen(evt)  { state.innerHTML = '<span style="color:green;">CONNECTED</span>';  }
    function onClose(evt) { state.innerHTML = '<span style="color:red;">DISCONNECTED</span>'; testWebSocket(); }  
    function onMessage(evt) 	{
      var result = window.eval(evt.data);
      if (result != undefined) {
        // console.log(String(result));
      }
      writeToScreen("RESPONSE:", "blue", evt.data);
    }
    function onError(evt)	{ writeToScreen("ERROR:", "red", evt.data); }  
    function doSend(message)	{ websocket.send(message); writeToScreen("SENT:", "green",  message);}
    function checkSubmit(e)	{ if (e && e.keyCode ==13) { doSend($('#msg').val()); }}

    function writeToScreen(tag, color, message) {
      //console.log(color + " " + message);
      if (message !== undefined) {
        output.innerHTML = "<span style='color: " + color
	    + ";'>" + tag + '</span> '
            + message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
      }
    }
    function clearOutput() {
      myterm.clear();
    }
    $( document ).ready(function() { init()});
  </script>
</head>

<body role="document">

  <div class="container theme-showcase" role="main">
    <div class="page-header">
      <h1>MiniTerm</h1>
      <p class="lead">running at  <%= $wsProtocol://$baseUrl %> </p>
    </div>
    
    <div style="margin: 5px 0px;">
      Status: <span id="state">Uninitialized</span>
      <button type="button" class="btn btn-default" onclick="clearOutput();">Clear Output</button>
    </div>

    <div class="row">
    <div class="well">
    <article>
        <header id="demo"><h2>Demo</h2></header>
        <p>This is simple demo using Tcl interpreter.
	  (If cursor is not blinking - click on terminal to activate it). 
	<p/>
	<select name="choose-kernel">
	<option value="task1" selected>Task 1</option>
	<option value="task2">Task 2</option>
	</select>
	<script language="javascript" type="text/javascript">
	var select = document.getElementById('choose-kernel');

	// return the index of the selected option
	alert(select.selectedIndex); // 1

	// return the value of the selected option
	alert(select.options[select.selectedIndex].value) // Second
 	</script>
	<button type="button" onclick="alert(kernel)">Click Me!</button>
        <div id="term_demo" data-kernel="demo"></div>
	<div id="output"></div>

	<script language="javascript" type="text/javascript">
    jQuery(function($, undefined) {
      $('#term_demo').terminal(function(command, term) {
        myterm = term;
        var kernel = $('#term_demo').data('kernel')
      //  var kernel = $('#choose-kernel option:selected').data('kernel')
        if (kernel === undefined) {kernel = '';}
        if (command !== '') {
           websocket.send(JSON.stringify(['eval', command, kernel]));
        } else {
           term.echo('');
        }
    }, {
        greetings: 'NX Interpreter',
        name: 'js_demo',
        height: 200,
        prompt: '% '});
    });
    </script>
      </article>

	</div>
      </div>
    </div>

</body>
</html>
